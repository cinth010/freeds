package thread_com_runnable;

public class ThreadRunnable  implements Runnable{
	
	private String nome;
	private int time_sleep;

	
	public ThreadRunnable (String nome, int timesleep) {
		this.nome = nome;
		this.time_sleep = timesleep;
		
		Thread t = new  Thread(this);
		t.start();
		
	}
	
	
	public void run() {
		long InicalTime = System.currentTimeMillis();
		System.out.println("***********************************");
		System.out.println(nome + " iniciou a prova!");
		System.out.println("***********************************");
		System.out.println("");
		
		try {
			
		for (int i=1; i<11; i++) {
			System.out.println("|" + nome + "|"+ " terminou a questão " + i);
			System.out.println("------------------------------");
			System.out.println("");
				Thread.sleep(time_sleep);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		long FinalTime = System.currentTimeMillis();
		
		long runTime = FinalTime - InicalTime;
		
		System.out.println("===============================");
		System.out.println( nome + " terminou a prova em " + runTime + " milisegundos");
		System.out.println("===============================");
		System.out.println("");
		
		
	
		
			
	}
	

}
